window.onload = function () {};

document.addEventListener('alpine:init', () => {

    console.log("Initializing Alpine.js...");

    Alpine.data('navbar', () => ({
        menuVisible: true,

        init() {},

        scroll() {
            document.querySelector(this.$el.dataset["scroll"]).scrollIntoView({
                behavior: 'smooth'
            });
        },

        toggleMenu() {
            this.menuVisible = !this.menuVisible;
        }
    }));
})